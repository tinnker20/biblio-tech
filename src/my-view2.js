/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import './shared-styles.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '@polymer/iron-image/iron-image.js';


class MyView2 extends PolymerElement {
  static get template() {
    return html`
      <style include="shared-styles">
        :host {
          display: block;

          padding: 10px;
        }
        .book-info {
          padding: 50px;
        }
        iron-image {
          width: 400px;
          height: 400px;
        }
      </style>
      <iron-ajax
          auto
          url="index.json"
          handle-as="json"
          on-response="handleResponse"
          last-response="{{jsonResponse}}"
          debounce-duration="300">
      </iron-ajax>
      <div class="card">
      <div>
            <iron-image sizing="contain" fade src="https://d1re4mvb3lawey.cloudfront.net/pg1017/cover.jpg"></iron-image>
      </div>
      <div class="book-info">
            <h1 class="book-title">{{jsonResponse.title}} <span class="book-year">{{jsonResponse.year}}</span> </h1>
            <p><span class="isbn">ISBN:{{jsonResponse.isbn}}</span>, <span class="book-author">Author:{{jsonResponse.contributors}}</span></p>
      </div>
      </div>
    `;
  }

}

window.customElements.define('my-view2', MyView2);
